<!DOCTYPE html>
<html lang="pt-BR">
<head>
    <meta charset="utf-8">
    <link rel="shortcut icon" href="assets/img/favicon.ico" type="image/x-icon">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" href="assets/css/bootstrap-flex.css">
    <link rel="stylesheet" href="assets/css/style.css" type="text/css" media="all">
    <script src="https://use.fontawesome.com/01f6939522.js"></script>
    <style>
        p.ind{
            text-indent: 2em;
        }
    </style>

    <title>Colégio Técnico de Limeira</title>
</head>
<body>
<?php include './assets/include/header.inc';?>

<div class="container" style="background-color: #FFFFFF">
    <div class="jumbotron" style="text-align: justify">
        <br>
        <h2 style="text-align: center">Professor, prepare-se para liderar uma expedição!</h2>
        <br>
        <p class="ind">Antes que você possa liderar uma expedição, precisará concluir alguns passos de preparação no seu dispositivo "professor" e também nos dispositivos "aluno" e garantir uma viagem suave.</p>
        <br>
        <p><b>1. Instale o Google Expeditions nos dispositivos na sala de aula. Os professores (guias) e os alunos (exploradores) usam o mesmo aplicativo.</b></p>
        <p class="ind">Para dispositivos Android: Vá para o aplicativo "Expedições" na Google Play Store. Uma alternativa é procurar "Expedições" em <a href="http://play.google.com/store/"class="text-danger">http://play.google.com/store/</a>.  Toque em "Instalar" para adicionar a aplicação ao dispositivo. Toque em "Abrir" ou toque no menu "Aplicações" para encontrar "Expedições" na sua lista de aplicações.</p>
        <p class="ind">Para dispositivos iOS: Acesse o aplicativo "Expedições" na iTunes Apps Store. Em alternativa, procure "Expedições" em <a href="https://itunes.apple.com" class="text-danger"> https://itunes.apple.com</a>. Toque em "Obter> Instalar" para adicionar a aplicação ao dispositivo.</p>
        <p><b>2. Verifique o seu Wi-Fi</b></p>
        <p class="ind">Lembre-se de que você precisará de uma conexão com a Internet para instalar o aplicativo ou fazer o download de uma expedição individual, mas você só precisa de uma rede Wi-Fi peer-to-peer para liderar uma expedição. Verifique com o seu administrador de TI para ver se sua rede Wi-Fi da escola está habilitada para rede peer-to-peer. Se não for, você pode usar um roteador autônomo ou executar um hotspot de um telefone.</p>
        <p class="ind">Ao fazer um tour ou iniciar sua própria expedição, verifique se o dispositivo está conectado à rede Wi-Fi que você configurou. Toque em "Definições" e depois em "Wi-Fi".Verifique se o Wi-Fi está ativado e se você está conectado a uma rede.</p>
        <p><b>3. Experimente uma demonstração</b></p>
        <p class="ind">Depois de instalar o "Expeditions", você pode seguir uma expedição de demonstração. Expedições é uma experiência de grupo com o professor atuando como guia e os alunos seguindo como exploradores. O passeio irá mostrar-lhe a diferença nas experiências. Você pode assistir ao tour no modo de tela estereoscópica usando um telefone e um visualizador de realidade virtual (VR) ou você pode vê-lo no modo de tela cheia em um telefone ou tablet sem um visualizador.</p>
        <p class="ind">Para visualizar a demonstração: Abra o aplicativo "Expedições". Se esta for a sua primeira vez no aplicativo, toque em "Ver o que é". Se você selecionou uma função anteriormente, toque em "Guia" ou "Explorer" na parte superior da tela e, em seguida, toque em "Ver o que é". Se você estiver usando um visualizador, coloque o telefone em seu visualizador. Se você não estiver usando um visualizador, toque em "Tela cheia" para assistir à demonstração no modo de tela cheia. Toque na seta para trás para sair da demonstração. Escolha o seu papel para iniciar uma expedição.</p>
        <p><b>4. Faça o download de uma expedição</b></p>
        <p class="ind">Você pode escolher entre centenas de expedições para compartilhar com sua classe. Você precisará de uma conexão à Internet para fazer o download de expedições individuais.</p>
        <p class="ind">Para fazer o download de uma expedição: No aplicativo "Expedições", certifique-se de que sua função é "guia". Se você selecionou anteriormente a função de explorador, toque em "Explorer" na parte superior da tela e, em seguida, toque em "Guia". Procure um título ou percorra a lista. Toque em uma expedição para começar a fazer o download. As expedições que você baixou terão uma marca de seleção branca no canto inferior direito da foto da capa.</p>
        <p><b>5. Começar uma expedição</b></p>
        <p class="ind">Depois de concluir as etapas acima, você está pronto para liderar uma expedição. Consulte "Começar uma expedição" para obter instruções.</p>
        <br>
        <p>Dúvidas:<a href="https://mail.google.com/mail/u/0/?view=cm&fs=1&tf=1&source=mailto&to=sau@cotil.unicamp.br" class="text-danger"> <b>E-mail para contato</b></a> </p>
    </div>
</div>

<?php include './assets/include/footer.inc';?>

<script src="assets/js/jquery-3.1.1.js"></script>
<script src="assets/js/tether.js"></script>
<script src="assets/js/bootstrap.js"></script>
</body>
</html>