<!DOCTYPE html>
<html lang="pt-BR">
<head>
    <meta charset="utf-8">
    <link rel="shortcut icon" href="assets/img/favicon.ico" type="image/x-icon">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" href="assets/css/bootstrap-flex.css">
    <link rel="stylesheet" href="assets/css/style.css" type="text/css" media="all">
    <script src="https://use.fontawesome.com/01f6939522.js"></script>
    <style>
        p.ind{
            text-indent: 2em;
        }
    </style>

    <title>Colégio Técnico de Limeira</title>
</head>
<body>
<?php include './assets/include/header.inc';?>

<div class="container" style="background-color: #FFFFFF">
    <div class="jumbotron" style="text-align: justify">
        <br>
        <h2 style="text-align: center">Mecânica</h2>
        <br>
        <p class="ind">Prepara o técnico para elaborar, detalhar e executar projetos; para controlar e executar inspeções de materiais, produtos, instrumentos e processos; elaborar orçamentos, levantamento de custos e relatórios; manipular máquinas, aparelhos e instrumentos; montar equipamentos e instalações produtivas, visando ao melhor aproveitamento dos recursos humanos, econômicos e ambientais dentro das empresas.</p>

        <p class="ind">O <b>Técnico em Mecânica</b> pode trabalhar em indústrias metalúrgicas, automobilísticas, alimentícias, de autopeças, de máquinas em geral, de equipamentos eletro-eletrônicos, em usinas, em vendas técnicas, etc. Poderá atuar como empreendedor na área, função que está em destaque no mercado de trabalho. </p>
        <br>
        <br>
        <br>
        <br>
        <br>
        <br>
        <br>
        <br>
        <br>

    </div>
</div>

<?php include './assets/include/footer.inc';?>

<script src="assets/js/jquery-3.1.1.js"></script>
<script src="assets/js/tether.js"></script>
<script src="assets/js/bootstrap.js"></script>
</body>
</html>