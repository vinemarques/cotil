<!DOCTYPE html>
<html lang="pt-BR">
  <head>
    <meta charset="utf-8">
    <link rel="shortcut icon" href="assets/img/favicon.ico" type="image/x-icon">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" href="assets/css/bootstrap-flex.css">
    <link rel="stylesheet" href="assets/css/style.css" type="text/css" media="all">
    <script src="https://use.fontawesome.com/01f6939522.js"></script>

    <title>Colégio Técnico de Limeira</title>
  </head>
    <body>
    <?php include './assets/include/header.inc';?>
        
    <div class="jumbotron" id="cotuca-top">
       <h1 class="intro-text" style="text-shadow: 2px 2px 4px #000000;">ENSINO MÉDIO E TÉCNICO DE QUALIDADE<br><small style="text-shadow: 2px 2px 4px #000000;">EXCELÊNCIA UNICAMP</small></h1>
    </div>

    <section id="card" style="background-color:#f3f3f3;">
    <div class="container" id="card">
	    <div class="jumbotron" style="background-color:#f3f3f3; margin-top: -34px">
            <div id="myCarousel" class="carousel slide bg-inverse" data-ride="carousel">
                <ol class="carousel-indicators">
                    <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
                    <li data-target="#myCarousel" data-slide-to="1"></li>
                    <li data-target="#myCarousel" data-slide-to="2"></li>
                </ol>
                <div class="carousel-inner" role="listbox">
                    <div class="carousel-item active">
                        <img class="d-block img-fluid ml-auto mr-auto img-rounded m-x-auto" src="./assets/img/car04.png" alt="First slide">
                        <div class="carousel-caption">
                            <h3>Faça das suas ideias uma startup!</h3>
                            <p>Programa Inova Jovem incentiva alunos inovadores</p>
                        </div>
                    </div>
                    <div class="carousel-item">
                        <img class="d-block img-fluid ml-auto mr-auto img-rounded m-x-auto" src="./assets/img/car01.jpg" alt="Second slide">
                        <div class="carousel-caption">
                            <h3>VITÓRIA NO TÊNIS DE MESA</h3>
                            <p>Equipe conquistou o 3º lugar nos Jogos Escolares do Estado de São Paulo</p>
                        </div>
                    </div>
                    <div class="carousel-item">
                        <img class="img-fluid d-block  ml-auto mr-auto img-rounded m-x-auto" src="./assets/img/car003.jpg" alt="Third slide">
                        <div class="carousel-caption">
                            <h3>ALUNOS APRESENTAM 999º EDIÇÃO DO COTIL ARTE</h3>
                            <p>Magnifico. Wonderful. Incredible. Maravilhoso.</p>
                        </div>
                    </div>
                </div>
                <a class="carousel-control prev left" href="#myCarousel" role="button" data-slide="prev">
                    <span class="carousel-control icon-prev" aria-hidden="true"></span>
                    <span class="sr-only">Previous</span>
                </a>
                <a class="carousel-control next right" href="#myCarousel" role="button" data-slide="next">
                    <span class="carousel-control icon-next" aria-hidden="true"></span>
                    <span class="sr-only">Next</span>
                </a>
            </div>
        </div>
    </div>
    </section>
    <br>
  <div class="container">
        <div class="section-header" style="text-align: center">
            <h2 class="dark-text"><a href="servicos.php" style="text-decoration: none; color: #373A3C;"> Central de Serviços</a></h2>
            <h6>Serviços oferecidos pelo Colégio para auxiliar os alunos:</h6>
        </div>
        <br>
        <div class="row destacados">
            <div class="col-md-3">
                <a href="eduroam.php" style="text-decoration: none; color: #373A3C;">
                <div style="text-align:center">
                    <i class="fa fa-wifi fa-5x" style="color: #217346;font-size: 100px; text-align: center" aria-hidden="true"></i>
                    <br>
                    <br>
                    <h4 style="text-align: match-parent;">
                        Rede Sem Fio</a>
                    </h4>
                    <br>
                </div>
            </div>

            <div class="col-md-3">
                <a href="http://www.cotil.unicamp.br/informatica/google.php" style="text-decoration: none; color: #373A3C;">
                <div style="text-align:center">
                    <i class="fa fa-graduation-cap fa-5x" style="color: #f4b400;font-size: 100px; align-items: center" aria-hidden="true"></i>
                    <br>
                    <br>
                    <h4 style="text-align: match-parent;">
                        Google Classroom</a>
                    </h4>
                    <br>
                </div>
            </div>

            <div class="col-md-3">
                <a href="microsoft.php" style="text-decoration: none;color: #373A3C;">
                <div style="text-align:center">
                    <i class="fa fa-windows fa-5x" style="font-size: 100px; color:#00ADEF;text-align: center" aria-hidden="true"></i>
                    <br>
                    <br>
                    <h4 style="text-align: match-parent">
                        Office 365</a></h4>
                    <br>
                </div>
            </div>

            <div class="col-md-3">
                <a href="http://www.cotil.unicamp.br/informatica/Informe.pdf" style="text-decoration: none; color: #373A3C;">
                <div style="text-align:center">
                    <i class="fa fa-file-text fa-5x" style="font-size: 100px; color:#B7472A;text-align: center" aria-hidden="true"></i>
                    <br>
                    <br>
                    <h4 style="text-align: match-parent;">
                        Notícias COTIL</a>
                    </h4>
                    <br>
                </div>
            </div>

        </div>
    </div>
    <br>
    <br>
    
            <section id="card" style="background-color:#f0f0f0;margin-bottom: -20px">
                <br><br>
                <h2 style="text-align: center">DEPOIMENTOS DE EX-ALUNOS</h2>
                <br>
                <div class="col-md-12 card-deck">
                        <div class="card col-md-4">
                            <div class="card-block" style="align-items: center">
                                <i class="fa fa-quote-left" aria-hidden="true"></i>
                                <p class="card-text" style="text-align: justify">Vivamus bibendum sollicitudin leo, ut sagittis elit fringilla id. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Praesent et libero nunc. Etiam consequat vulputate ante id vulputate. Cras sit amet leo tincidunt, vestibulum tortor ac...</p>
                                <p style="text-align: right"><a href="#" class="text-danger"><b>[Leia mais]</b></a></p>
                                <h5 class="card-title" style="text-align: right"><a href="#" style="color: #373A3C;">Fulano</a></h5>
                                <h6 class="card-subtitle" style="text-align: right">ex-aluno de Informática</h6>
                            </div>
                        </div>

                        <div class="card col-md-4">
                            <div class="card-block">
                                <i class="fa fa-quote-left" aria-hidden="true"></i>
                                <p class="card-text" style="text-align: justify">Pellentesque scelerisque bibendum arcu. Curabitur lobortis cursus pulvinar. Sed tristique ipsum ultricies sit amet. Integer porttitor mollis ligula, et porttitor ligula scelerisque sit amet. Cras non mollis libero. Fusce pretium fermentum ante, eget fringilla ipsum volutpat a...</p>
                                <p style="text-align: right"><a href="#" class="text-danger"><b>[Leia mais]</b></a></p>
                                <h5 class="card-title" style="text-align: right"><a href="#" style="color: #373A3C;">Beltrano</a></h5>
                                <h6 class="card-subtitle" style="text-align: right">ex-aluno de Qualidade</h6>
                            </div>
                        </div>

                        <div class="card col-md-4">
                            <div class="card-block">
                                <i class="fa fa-quote-left" aria-hidden="true"></i>
                                <p class="card-text" style="text-align: justify">Sed finibus mi mauris, ac imperdiet dolor euismod tincidunt. Pellentesque felis leo, varius eget orci vel, hendrerit mattis tellus. Etiam porttitor sem et odio tempus commodo. Praesent volutpat auctor ante quis rutrum. Sed vestibulum consectetur nisl, sed ultrices...</p>
                                <p style="text-align: right"><a href="#" class="text-danger"><b>[Leia mais]</b></a></p>
                                <h5 class="card-title" style="text-align: right"><a href="#" style="color: #373A3C;">Sicrano</a></h5>
                                <h6 class="card-subtitle" style="text-align: right">ex-aluno de Edificações</h6>
                            </div>
                        </div>
                </div>
                <p style="text-align: center">Veja mais <a href="#" class="text-danger">depoimentos de ex-alunos aqui.<br><br></a> </p>
        </section>

    <?php include './assets/include/footer.inc';?>

    <script src="assets/js/jquery-3.1.1.js"></script>
    <script src="assets/js/tether.js"></script>
    <script src="assets/js/bootstrap.js"></script>
  </body>
  </html>
