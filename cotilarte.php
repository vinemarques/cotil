<!DOCTYPE html>
<html lang="pt-BR">
<head>
    <meta charset="utf-8">
    <link rel="shortcut icon" href="assets/img/favicon.ico" type="image/x-icon">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" href="assets/css/bootstrap-flex.css">
    <link rel="stylesheet" href="assets/css/style.css" type="text/css" media="all">
    <script src="http://use.fontawesome.com/01f6939522.js"></script>
    <style>
        p.ind{
            text-indent: 2em;
        }
    </style>

    <title>Colégio Técnico de Limeira</title>
</head>
<body>
<?php include './assets/include/header.inc';?>

<div class="container" style="background-color: #FFFFFF">
    <div class="jumbotron" style="text-align: justify">
    <br>
        <h2 style="text-align: center">Cotil Arte</h2>
        <br>

<p class="ind">O <b>COTIL ARTE</b> é um evento artístico-cultural coordenado pelo Departamento de Humanas
    do Colégio Técnico de Limeira &ndash; COTIL - UNICAMP, desde 1996. Atualmente, o
    evento envolve cerca de 1.500 alunos, em diversas modalidades, com exposição de trabalhos no campus I de Limeira e apresentação dos finalistas de composição musical, esquete e performance músico-corporal no Teatro Municipal de Limeira, oportundiade em que acontece a premiação dos melhores trabalhos.
</p>

<p class="ind">
    Desde a primeira edição, o COTIL ARTE tem se notabilizado pelo
    envolvimento com a comunidade interna e externa, constituindo-se num evento de
    repercussão em nossa cidade, na Unicamp e também na região.
</p>

<p class="ind">
    Sua realização extrapola a mera apresentação do evento em si, tendo
    um caráter pedagógico, seja no desenvolvimento e aprimoramento de habilidades
    artísticas, seja no fomento à pesquisa de temas complementares à formação
    técnico-acadêmica de nossos alunos, para a consolidação dos valores defendidos
    por esta instituição de ensino.
</p>
<p class="ind">
    Devido à sua importância, foi incluído no calendário cultural oficial de Limeira, por lei aprovada pela Câmara Municipal, de autoria do vereador Prof. Farid Zaine.
</p>

<p>
    <b>Conheça um pouco mais do Histórico do Cotil Arte:</b>
    <a href="http://www.youtube.com/embed/RfgiqP68wnw" class="text-danger"><b>[Clique aqui]</b></a>


<p>
    <b>Clique no ano para visualizar detalhes das últimas edições:</b>
</p>

<a href="http://www.cotil.unicamp.br/cotilarte/2006/index2.htm" class="text-danger">Ano de 2006</a>
<br>
<br>
<a href="http://www.cotil.unicamp.br/cotilarte/2007/index.htm" class="text-danger">Ano de 2007</a>
<br>
<br>
<a href="http://wwww.cotil.unicamp.br/cotilarte/2008/index.htm" class="text-danger">Ano de 2008</a>
<br>
<br>
<a href="http://www.cotil.unicamp.br/cotilarte/2009/index.html" class="text-danger">Ano de 2009</a>
<br>
<br>
<a href="http://www.cotil.unicamp.br/cotilarte/2010/index.html" class="text-danger">Ano de 2010</a>
<br>
<br>
<a href="http://www.cotil.unicamp.br/cotilarte/2011/index.html" class="text-danger">Ano de 2011</a>
<br>
<br>
<a href="http://www.cotil.unicamp.br/cotilarte/2012/index.html" class="text-danger">Ano de 2012</a>
<br>
<br>
<a href="http://www.cotil.unicamp.br/cotilarte/2013/index.html" class="text-danger">Ano de 2013</a>
<br>
<br>
<a href="http://www.cotil.unicamp.br/cotilarte/2014/index.html" class="text-danger">Ano de 2014</a>
<br>
<br>
<a href="http://www.cotil.unicamp.br/cotilarte/2015/index.html" class="text-danger">Ano de 2015</a>
<br>

<br>
</div>
</div>

<?php include './assets/include/footer.inc';?>

<script src="assets/js/jquery-3.1.1.js"></script>
<script src="assets/js/tether.js"></script>
<script src="assets/js/bootstrap.js"></script>
</body>
</html>