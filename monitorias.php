<!DOCTYPE html>
<html lang="pt-BR">
<head>
    <meta charset="utf-8">
    <link rel="shortcut icon" href="assets/img/favicon.ico" type="image/x-icon">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" href="assets/css/bootstrap-flex.css">
    <link rel="stylesheet" href="assets/css/style.css" type="text/css" media="all">
    <script src="https://use.fontawesome.com/01f6939522.js"></script>

    <title>Colégio Técnico de Limeira</title>
</head>
<body>
<?php include './assets/include/header.inc';?>

<div class="container" style="background-color: #FFFFFF">
    <div class="jumbotron" style="text-align: center">
        <br>
        <h2 style="text-align: center">Monitorias</h2>
        <br>
        <br>
        <h4><b>Horários das Monitorias</b><br><br></h4>
        <u><b><a href="http://www.cotil.unicamp.br/arquivos/monitoria/monitoria_exatas.pdf" class="text-danger">Monitorias de Exatas</a></u></b><br><br>
        <u><b><a href="http://www.cotil.unicamp.br/arquivos/monitoria/monitoria_humanas.pdf" class="text-danger">Monitorias de Humanas</a></u></b><br><br>
        <u><b><a href="http://www.cotil.unicamp.br/arquivos/monitoria/monitoria_tecnicos.pdf" class="text-danger">Monitorias do Técnico</a></u></b><br><br>
        <u><b><a href="http://www.cotil.unicamp.br/arquivos/monitoria/agendamento.pdf" class="text-danger"> Horários de Agendamento</a></u></b><br><br>
        <br>
        <br>
        <br>
        <br>
    </div>
</div>

<?php include './assets/include/footer.inc';?>

<script src="assets/js/jquery-3.1.1.js"></script>
<script src="assets/js/tether.js"></script>
<script src="assets/js/bootstrap.js"></script>
</body>
</html>