<!DOCTYPE html>
<html lang="pt-BR">
  <head>
    <meta charset="utf-8">
    <link rel="shortcut icon" href="assets/img/favicon.ico" type="image/x-icon">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" href="assets/css/bootstrap-flex.css">
    <link rel="stylesheet" href="assets/css/style.css" type="text/css" media="all">
    <script src="https://use.fontawesome.com/01f6939522.js"></script>
      <style>
          p{
              text-indent: 2em;
          }
      </style>

    <title>Colégio Técnico de Limeira</title>
  </head>
    <body>
    <?php include './assets/include/header.inc';?>

    <div class="container" style="background-color: #FFFFFF">
	    <div class="jumbotron" style="text-align: justify">
            <br>
            <h2 style="text-align: center">Nossa História</h2>
            <br>
            <p> O Colégio Técnico de Limeira - COTIL, da Universidade Estadual de Campinas, foi criado pela Lei Estadual no 7.655, de 28 de dezembro de 1962, e autorizado a ser instalado e a entrar em funcionamento pela Resolução C.E.E. no 46/66 e Deliberação C.E.E. no 12/70, Diário Oficial de 29/01/72, página 21. A instalação foi em 24 de abril de 1967, dia em que se comemora seu aniversário. Inicialmente recebeu o nome de Colégio Técnico e Industrial de Limeira, tendo como sua mantenedora a Universidade Estadual de Campinas. Iniciou seu funcionamento nas instalações do Ginásio Estadual Industrial Trajano Camargo de Limeira e passou para as novas instalações no "campus" de Limeira da UNICAMP, tendo sido o prédio inaugurado em 9 de setembro de 1973. O primeiro Diretor foi o Professor Manoel da Silva e o atual é o Professor Paulo Sérgio Saran.</p>

            <p>O COTIL oferece Ensino Médio, preparando os alunos também para o vestibular, com a qualidade UNICAMP. Obteve o primeiro lugar no ENEM (das escolas públicas) nos últimos anos, pois incentiva o raciocínio e a capacidade de aprender. Além disso, tem conquistado medalhas de ouro, prata e bronze nas olimpíadas de Matemática e Física e grande êxito no acesso ao ensino superior.</p>

            <p>Atualmente, forma técnicos em Edificações, Enfermagem, Geodésia e Cartografia, Informática, Mecânica e Qualidade e Produtividade, os quais se inserem no mercado de trabalho com segurança.</p>

            <p>Tem em seu quadro 89 professores e 1500 alunos.</p>

            <p>O COTIL oferece cursos de extensão, de aperfeiçoamento, de especialização, visando conduzir ao permanente desenvolvimento do aperfeiçoamento profissional.</p>

            <p>O COTIL mantém cursos diurno e noturno, atendendo à comunidade e região. O número de candidatos tem aumentado a cada ano, numa busca de melhores condições de formação técnica.</p>

            <p>O objetivo geral do COTIL é proporcionar ao educando formação necessária para o desenvolvimento de suas potencialidades como elemento de auto-realização, preparação para o trabalho e exercício consciente da cidadania.</p>

            <p>A missão do COTIL é desenvolver as competências do educando para que se torne um cidadão capaz de superar, de forma crítica, ética e criativa, os desafios do mundo globalizado, interagindo com segurança na sociedade.</p>

            <p>Nos últimos anos, investimos de forma significativa no Ensino Médio, com a implementação dos Simulados de Avaliação, Simulados do ENEM, Simulados de Redação, SimVest (Simulados de Vestibulares) e Simpósio de Orientação e Oportunidades Profissionais.</p>

            <p>Os cursos técnicos receberam grandes investimentos, com a modernização dos laboratórios e revisão e atualização dos Planos de Cursos.</p>

            <p>O COTIL investe no potencial artístico do aluno, tendo o COTIL ARTE seu maior evento artístico-cultural alcançado repercussão regional e incluído no calendário de eventos oficiais da cidade, por lei aprovada pela Câmara Municipal, de autoria do Vereador Professor Farid Zaine, e realiza sua premiação no Teatro Vitória.</p>

            <p>É nossa meta continuar expandindo o número de vagas, criando cursos, formando cidadãos responsáveis e promovendo educação profissional para atender à sociedade, a fim de que o educando participe do mercado de trabalho da cidade, da região e do país.</p>

            <p>Desde 2003 ocorre a descentralização do EXAME DE SELEÇÃO,realizado atualmente em 7 cidades da região: Limeira, Campinas, Americana, Araras, Cosmópolis, Piracicaba e Rio Claro.</p>

            <p>Faz parte do projeto desta gestão a criação de linhas de pesquisas em sintonia com a missão da Unicamp: ensino, extensão e pesquisa.</p>

            <p>Formamos o profissional de hoje, pensando no homem de amanhã.</p>

            A Direção.

        </div>
    </div>

    <?php include './assets/include/footer.inc';?>

    <script src="assets/js/jquery-3.1.1.js"></script>
    <script src="assets/js/tether.js"></script>
    <script src="assets/js/bootstrap.js"></script>
  </body>
  </html>