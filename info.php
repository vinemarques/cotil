<!DOCTYPE html>
<html lang="pt-BR">
<head>
    <meta charset="utf-8">
    <link rel="shortcut icon" href="assets/img/favicon.ico" type="image/x-icon">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" href="assets/css/bootstrap-flex.css">
    <link rel="stylesheet" href="assets/css/style.css" type="text/css" media="all">
    <script src="https://use.fontawesome.com/01f6939522.js"></script>
    <style>
        p.ind{
            text-indent: 2em;
        }
    </style>

    <title>Colégio Técnico de Limeira</title>
</head>
<body>
<?php include './assets/include/header.inc';?>

<div class="container" style="background-color: #FFFFFF">
    <div class="jumbotron" style="text-align: justify">
        <br>
        <h2 style="text-align: center">Informática</h2>
        <br>
        <p class="ind">A formação profissional sólida e flexível no campo da informática permite ao técnico trabalhar nos mais variados segmentos e avançar em consonância com a evolução, sofisticação e dinamismo das tecnologias contemporâneas.</p>

        <p class="ind">O <b>Técnico em Informática</b> atuará nas áreas de análise e projetos de sistemas em computadores, automação administrativa, comercial e industrial, telecomunicações e prestação de serviços</p>

        <p class="ind">O profissional desta área deverá estar consciente para consolidar uma utilização ética da informática, valorizando a aplicação de técnicas apropriadas que garantam a qualidade da aplicação das <a href="./adm/index.php" style="text-decoration: none; color: black;">tecnologias.</a></p>
        <br>
        <br>
        <br>
        <br>
        <br>
        <br>
        <br>
        <br>

    </div>
</div>

<?php include './assets/include/footer.inc';?>

<script src="assets/js/jquery-3.1.1.js"></script>
<script src="assets/js/tether.js"></script>
<script src="assets/js/bootstrap.js"></script>
</body>
</html>