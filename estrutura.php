<!DOCTYPE html>
<html lang="pt-BR">
  <head>
    <meta charset="utf-8">
    <link rel="shortcut icon" href="assets/img/favicon.ico" type="image/x-icon">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" href="assets/css/bootstrap-flex.css">
    <link rel="stylesheet" href="assets/css/style.css" type="text/css" media="all">
    <script src="https://use.fontawesome.com/01f6939522.js"></script>

    <title>Colégio Técnico de Limeira</title>
  </head>
    <body>
    <?php include './assets/include/header.inc';?>

    <div class="container" style="background-color: #FFFFFF">
	    <div class="jumbotron" style="text-align: justify">
            <br>
            <h2 style="text-align: center">Estrutura Física</h2>
            <br>

            <p>Aos nossos alunos são oferecidos:</p>

            <p><b>AMO - Ambulatório Médico-odontológico: </b> Localizado no campus, para atendimento de emergência durante o período de aula do aluno.</p>

            <p><b>Biblioteca Informatizada: </b> É um setor pedagógico-complementar para auxiliar o aluno em suas pesquisas e consultas. As instalações da biblioteca são amplas e arejadas.</p>

            <p><b>Cadastro de Moradia: </b> Aos alunos que não residem em Limeira o COTIL disponibiliza um cadastro de imóveis cujos proprietários oferecem locação. Esse serviço de apoio tem o fim único de agilizar a efetivação entre as partes interessadas.</p>

            <p><b>Cantina: </b> São servidos lanches, salgados, doces, sucos e refrigerantes para os cursos diurno e noturno. Local de encontro e bate-papos.</p>

            <p><b>Chefias de Departamento: </b> Para coordenação das atividades dos cursos, verificação da adequação dos currículos e atendimento a alunos e professores.</p>

            <p><b>Gráfica: </b> Cópia e venda de apostilas e xerox a preço acessível. Faz também serviço de encadernação.</p>

            <p><b>Helpmóvel: </b> Convênio patrocinado pela APM, para atendimento médico de urgência e emergência.</p>

            <p><b>Laboratórios: </b> Informática I e II, Edificações e Geodésia, Química e Biologia, Enfermagem, Produção Mecânica, CAD, Metrologia e Topografia.</p>

            <p><b>Locação De Armários: </b> O Colégio disponibiliza armários com locação anual, para guarda de materiais, mediante assinatura de contrato e termo de responsabilidade.</p>

            <p><b>Monitoria: </b> O COTIL oferece um serviço de aulas de reforço com alunos monitores, escolhidos por seu desempenho e orientados por professores.</p>

            <p><b>Praça Esportiva: </b> Quadras, campo de futebol, sala de jogos (dama, xadrez, tênis-de-mesa).</p>

            <p><b>Restaurante: </b> São servidos almoço e jantar a preços subsidiados, de segunda a sexta-feira, pelo sistema de bandejão.</p>

            <p><b>Setor de Estágios: </b> Para encaminhamento e orientação no que se refere ao estágio profissional supervisionado e ao mercado de trabalho.</p>

            <p><b>SimEnem: </b> Simulado para os alunos concluintes do ensino médio e interessados em treinar para o Exame Nacional do Ensino Médio.</p>

            <p><b>SimVest: </b> Simulado para o Vestibular Simulado para os alunos interessados em treinar para o vestibular.</p>

            <p><b>Simulado de Redação: </b> Tem o objetivo de contribuir para um melhor desempenho do aluno nos vestibulares; é uma oportunidade para ele refletir sobre suas condições de elaboração de um texto.</p>

            <p><b>Visitas Técnicas: </b> Durante o ano são agendadas visitas de caráter didático-pedagógico a empresas, feiras e hospitais. A participação do aluno nesses eventos é de suma importância, oportunidade de contato direto com o mercado de trabalho.</p>
	    </div>
    </div>

    <?php include './assets/include/footer.inc';?>

    <script src="assets/js/jquery-3.1.1.js"></script>
    <script src="assets/js/tether.js"></script>
    <script src="assets/js/bootstrap.js"></script>
  </body>
  </html>