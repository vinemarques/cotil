<!DOCTYPE html>
<html lang="pt-BR">
<head>
    <meta charset="utf-8">
    <link rel="shortcut icon" href="assets/img/favicon.ico" type="image/x-icon">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" href="assets/css/bootstrap-flex.css">
    <link rel="stylesheet" href="assets/css/style.css" type="text/css" media="all">
    <script src="https://use.fontawesome.com/01f6939522.js"></script>

    <title>Colégio Técnico de Limeira</title>
</head>
<body>
<?php include './assets/include/header.inc';?>

<div class="container" style="background-color: #FFFFFF">
    <div class="jumbotron">
    <br>
        <h2 style="text-align: center">Concursos</h2>
        <br>

    <h4>Processos Seletivos Temporários</h4><br>

    <p>Não há nenhum processo aberto no momento.</p><br><br>

        <h4>Processos Seletivos Públicos</h4>
    <br>

    <b>Área: </b>Mecânica &nbsp;&nbsp;
    <a href="http://www.cotil.unicamp.br/concursos/permanentes/Edital%20002-2016.pdf" class="text-danger">&nbsp;&nbsp;<u>Edital 002-2016</u></a><br><br>

    <b>Área: </b>Informática <a href="http://www.cotil.unicamp.br/concursos/permanentes/Edital%20001-2016.pdf" class="text-danger">&nbsp; <u>Edital 001-2016</u></a>

    <a href="http://www.cotil.unicamp.br/concursos/permanentes/comunicados/Comunicado%20Edital%20001-2016.pdf" class="text-danger">&nbsp; <u>Comunicado</u></a>

    <a href="http://www.cotil.unicamp.br/concursos/permanentes/classificados/classificados001-2016.pdf" class="text-danger">&nbsp; <u>Classificação</u></a><br>

    <br><br>

        <b><a href="http://www.cotil.unicamp.br/concursos/editais.php#permanentes" class="text-danger"><u>Informações Adicionais</u></a></b>

</div>
</div>
<br>

<?php include './assets/include/footer.inc';?>

<script src="assets/js/jquery-3.1.1.js"></script>
<script src="assets/js/tether.js"></script>
<script src="assets/js/bootstrap.js"></script>
</body>
</html>