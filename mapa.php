<!DOCTYPE html>
<html lang="pt-BR">
  <head>
    <meta charset="utf-8">
    <link rel="shortcut icon" href="assets/img/favicon.ico" type="image/x-icon">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" href="assets/css/bootstrap-flex.css">
    <link rel="stylesheet" href="assets/css/style.css" type="text/css" media="all">


    <title>Colégio Técnico de Limeira</title>
    <script src="https://use.fontawesome.com/01f6939522.js"></script>  
  </head>
    <body>
	<?php include './assets/include/header.inc';?>
    <br>
    <br>
    <br>

    <article>
	<h2 style="text-align:center">Nossa localização</h2>
	<br>
	<div class="container formataContainer">     
      <iframe src="https://www.google.com/maps/embed?pb=!1m16!1m12!1m3!1d7368.865253056402!2d-47.428407210144385!3d-22.56291709046009!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!2m1!1sRua+Paschoal+Marmo%2C+1888+-+Jardim+Nova+Italia%2C+Limeira+-+SP!5e0!3m2!1spt-BR!2sbr!4v1493684966363" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
	</div>
	</article>
	<br>
    <br>
    <br>

    <?php include './assets/include/footer.inc';?>

	    <script src="assets/js/jquery-3.1.0.js"></script>
	    <script src="assets/js/tether.js"></script>
	    <script src="assets/js/bootstrap.js"></script>
	</body>
</html>