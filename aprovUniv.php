<!DOCTYPE html>
<html lang="pt-BR">
<head>
    <meta charset="utf-8">
    <link rel="shortcut icon" href="assets/img/favicon.ico" type="image/x-icon">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" href="assets/css/bootstrap-flex.css">
    <link rel="stylesheet" href="assets/css/style.css" type="text/css" media="all">
    <script src="https://use.fontawesome.com/01f6939522.js"></script>

    <title>Colégio Técnico de Limeira</title>
</head>
<body>
<?php include './assets/include/header.inc';?>

<div class="container" style="background-color: #FFFFFF">
    <div class="jumbotron">
        <br>
        <h2 style="text-align: center">Alunos Concluintes</h2>
        <h4 style="text-align: center">Pesquisa Sobre Vaga na Universidade</h4>
        <br>
        <p style="text-align: justify; text-indent: 2em">O COTIL considera os egressos parte de sua comunidade e deseja divulgar suas
            conquistas acadêmicas.</p>
        <p style="text-align: justify; text-indent: 2em">Caso seja um ex-aluno do COTIL, pedimos sua colaboração, respondendo ao
            questionário. Esclarecemos que todas as informações serão mantidas em sigilo,
            apenas dados do vestibular serão divulgados neste site, com autorização do
            aluno.</p>
            <u><a href="http://www.cotil.unicamp.br/universidade/consulta.php" class="text-danger">Consultar Andamento da Pesquisa</a></u>
        <br><br>

        <div class="card">
            <div class="card-block">
                <h5> Dados de sua conquista no vestibular: </h5>
                <br>
                <form action="#act=aprovado">
                    <div class="form-group">
                        <label for="nome"><b>Nome completo:</b></label>
                        <input type="text" class="form-control" id="nome" aria-describedby="nameHelp" placeholder="Fulano da Silva">
                        <small id="nameHelp" class="form-text text-muted">Nome Completo.</small>
                    </div>
                    <div class="form-group">
                        <label for="phone"><b>Telefone:</b></label>
                        <input type="phone" class="form-control" id="phone" aria-describedby="phoneHelp" placeholder="19 999999999">
                        <small id="phoneHelp" class="form-text text-muted">Fixo ou Móvel, acompanhando o código de área.</small>
                    </div>
                    <div class="form-group">
                        <label for="documento"><b>Informe seu RG:</b></label>
                        <input type="text" class="form-control" id="documento" aria-describedby="docHelp" placeholder="15000 ou 555555555">
                        <small id="docHelp" class="form-text text-muted">RG sem pontos ou traços.</small>
                    </div>
                    <div class="form-group">
                        <label for="email"><b>Email:</b></label>
                        <input type="email" class="form-control" id="email" aria-describedby="emailHelp" placeholder="fulano@unicamp.br">
                        <small id="emailHelp" class="form-text text-muted">Se você não informar seu e-mail, não receberá o aviso de que seu documento está pronto.</small>
                    </div>
                    <div class="form-group">
                        <label for="ct"><b>Curso Técnico:</b></label>
                        <select class="form-control" id="ct" name="ct">
                            <option disabled selected value>Selecione um curso técnico</option>
                            <option value="Medio">Ensino Médio</option>
                            <option value="Informatica">Técnico em Informática</option>
                            <option value="Edificacoes">Técnico em Edificações</option>
                            <option value="Enfermagem">Técnico em Enfermagem</option>
                            <option value="Geodesia">Técnico Geodésia</option>
                            <option value="Mecanica">Técnico em Mecânica</option>
                            <option value="Qualidade">Técnico em Qualidade</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="periodo"><b>Periodo:</b></label>
                        <select name="periodo" class="form-control" id="periodo">
                            <option disabled selected value>Selecione um período</option>
                            <option value="Diurno">Diurno</option>
                            <option value="Noturno">Noturno</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="ano"><b>Ano de Conclusão do Médio:</b></label>
                                <input class="form-control" type="number" max="2016" value="2016" id="ano" aria-describedby="yearHelp" placeholder=""
                        </div>
                    </div>
                    <br>
                    <br>
                    <div>
                    <h5> Dados das Universidades: </h5>
                    </div>
                    <br>
                    <div class="form-group">
                        <h5>Universidade 1</h5>
                        <label for="uni1"><b>Qual Universidade?</b></label>
                        <input type="text" class="form-control" id="uni1" aria-describedby="nameHelp">
                    </div>
                    <div class="form-group">
                        <label for="c1"><b>Qual curso?</b></label>
                        <input type="text" class="form-control" id="c1" aria-describedby="nameHelp">
                    </div>
                    <div class="form-group">
                        <label for="cl1"><b>Classificação:</b></label>
                        <input type="text" class="form-control" id="cl1" aria-describedby="nameHelp">
                    </div>
                    <br>
                    <br>
                    <div class="form-group">
                        <h5>Universidade 2</h5>
                        <label for="uni2"><b>Qual Universidade?</b></label>
                        <input type="text" class="form-control" id="uni2" aria-describedby="nameHelp">
                    </div>
                    <div class="form-group">
                        <label for="c2"><b>Qual curso?</b></label>
                        <input type="text" class="form-control" id="c2" aria-describedby="nameHelp">
                    </div>
                    <div class="form-group">
                        <label for="cl2"><b>Classificação:</b></label>
                        <input type="text" class="form-control" id="cl2" aria-describedby="nameHelp">
                    </div>
                    <br>
                    <br>
                    <div class="form-group">
                        <h5>Universidade 3</h5>
                        <label for="uni3"><b>Qual Universidade?</b></label>
                        <input type="text" class="form-control" id="uni3" aria-describedby="nameHelp">
                    </div>
                    <div class="form-group">
                        <label for="c3"><b>Qual curso?</b></label>
                        <input type="text" class="form-control" id="c3" aria-describedby="nameHelp">
                    </div>
                    <div class="form-group">
                        <label for="cl3"><b>Classificação:</b></label>
                        <input type="text" class="form-control" id="cl3" aria-describedby="nameHelp">
                    </div>
                    <br>
                    <div class="form-group">
                        <label for="maisde"><b>Em caso de mais de uma aprovação, qual foi sua escolha?</b></label>
                        <select name="maisde" class="form-control" id="maisde">
                            <option disabled selected value>Selecione uma universidade</option>
                            <option value="Universidade 1">Universidade 1</option>
                            <option value="Universidade 2">Universidade 2</option>
                            <option value="Universidade 3">Universidade 3</option>
                        </select>
                    </div>
                    <fieldset class="form-group" >
                        <label><b>O COTIL pode publicar seu resultado? </b></label>
                        <div class="form-check">
                            <label class="form-check-label">
                                <input type="radio" class="form-check-input" name="optionsRadios" id="optionsRadios1" value="Sim" checked>Sim</label>
                        </div>
                        <div class="form-check">
                            <label class="form-check-label">
                                <input type="radio" class="form-check-input" name="optionsRadios" id="optionsRadios2" value="Não">Não</label>
                        </div>
                        <div class="form-check">
                            <label><b>Verificação:</b></label>
                            <div class="g-recaptcha" data-sitekey="6LcbuiEUAAAAAO5xeGHnuhvl8tdYB1QByvm1HBz1"></div>
                        </div>
                    </fieldset>
                    <button type="submit" class="btn btn-danger active">Enviar Solicitação</button>
                </form>
            </div>
        </div>
    </div>
</div>

<?php include './assets/include/footer.inc';?>

<script src="assets/js/jquery-3.1.1.js"></script>
<script src="assets/js/tether.js"></script>
<script src="assets/js/bootstrap.js"></script>
<script src='https://www.google.com/recaptcha/api.js'></script>
</body>
</html>
