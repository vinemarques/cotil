<!DOCTYPE html>
<html lang="pt-BR">
  <head>
    <meta charset="utf-8">
    <link rel="shortcut icon" href="assets/img/favicon.ico" type="image/x-icon">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" href="assets/css/bootstrap-flex.css">
    <link rel="stylesheet" href="assets/css/style.css" type="text/css" media="all">
    <script src="https://use.fontawesome.com/01f6939522.js"></script>   

    <title>Colégio Técnico de Limeira</title>
  </head>
    <body>
    <?php include './assets/include/header.inc';?>

    <div class="container" style="background-color: #FFFFFF">
	    <div class="jumbotron">
            <br>
            <h2 style="text-align: center">Contato</h2>
            <br>
            <div class="card-deck">
                <div class="card">
                    <div class="card-block">
                        <p><b>Preencha todos os campos a seguir e nos envie um email</b></p>
                        <form action="#">
                            <div class="form-group">
                                <label for="nome"><b>Nome:</b></label>
                                <input type="text" class="form-control" id="nome" aria-describedby="emailHelp" placeholder="Fulano da Silva">
                                <small id="nameHelp" class="form-text text-muted">Nome Completo.</small>
                            </div>
                            <div class="form-group">
                                <label for="email"><b>Email:</b></label>
                                <input type="email" class="form-control" id="email" placeholder="fulano@unicamp.br">
                            </div>
                            <div class="form-group">
                                <label for="assunto"><b>Assunto:</b></label>
                                <input type="text" class="form-control" id="assunto" placeholder="Assunto">
                            </div>
                            <div class="form-group">
                                <label for="mensagem"><b>Mensagem:</b></label>
                                <textarea class="form-control" rows="10" id="mensagem"></textarea>
                            </div>
                            <div class="form-group">
                                <label><b>Verificação:</b></label>
                                <div class="form-group g-recaptcha" data-sitekey="6LdBuCEUAAAAAF-V_a70W0O9McPt9LwbLj28Bgmu""></div>
                            </div>
                            <br>
                            <button type="submit" class="btn btn-danger active">Enviar Email</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <?php include './assets/include/footer.inc';?>

    <script src="assets/js/jquery-3.1.1.js"></script>
    <script src="assets/js/tether.js"></script>
    <script src="assets/js/bootstrap.js"></script>
    <script src='https://www.google.com/recaptcha/api.js'></script>
  </body>
  </html>