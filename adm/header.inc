<header xmlns:padding="http://www.w3.org/1999/xhtml" xmlns:display="http://www.w3.org/1999/xhtml">
    <nav class="navbar navbar-light navbar-toggler-icon navbar-fixed-top" role="banner" style="border-radius:0px;background-color:#ebebeb;">
        <div class="row" style="display:flex; padding: 0px 0px 0px 0px;">

            <div class="col-md-3 col-sm-12">

                    <img src="../assets/img/unicamp-logo.png" style="width: 35px; height: auto;" class="navbar-brand d-inline-block align-top">


                    <img src="../assets/img/cotil-logo.png" style="width: 80px; height: auto;" class="navbar-brand d-inline-block align-top">


                <button id="nav-toggle-button" class="navbar-toggler hidden-md-up collapsed" type="button" data-toggle="collapse" style="float: right" data-target="#menu-sanduiche" aria-expanded="false">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
            </div>

            <div class="col-md-9 col-sm-0">
                <div class="navbar-toggleable-sm collapse" id="menu-sanduiche" aria-expanded="false">

                    <ul class="nav navbar-nav">
                        <li class="nav-item dropdown">
                            <a href="#" class="nav-link dropdown-toggle" data-toggle="dropdown">Homepage</a>
                            <div class="dropdown-menu">
                                <a href="homepage.php?action=banner" class="dropdown-item">Banner</a>
                                <a href="homepage.php?action=slider" class="dropdown-item">Slider</a>
                                <a href="homepage.php?action=serv" class="dropdown-item">Serviços</a>
                                <a href="homepage.php?action=depos" class="dropdown-item">Depoimentos</a>
                                <a href="homepage.php?action=restore" class="dropdown-item">Restaurar</a>
                            </div>
                        </li>

                        <li class="nav-item dropdown">
                            <a href="#" class="nav-link dropdown-toggle" data-toggle="dropdown">Páginas CT</a>
                            <div class="dropdown-menu">
                                <a href="./info.php" class="dropdown-item">Informática</a>
                                <a href="./mec.php" class="dropdown-item">Mecânica</a>
                                <a href="./enf.php" class="dropdown-item">Enfermagem</a>
                                <a href="http://www.cotil.unicamp.br/dcgeo/index.html" class="dropdown-item">Edificações</a>
                                <a href="http://www.cotil.unicamp.br/dcgeo/index.html" class="dropdown-item">Geodésia e Cartografia</a>
                                <a href="./qua.php" class="dropdown-item">Qualidade</a>
                            </div>
                        </li>

                        <li class="nav-item dropdown">
                            <a href="#" class="nav-link dropdown-toggle" data-toggle="dropdown">Formulários</a>
                            <div class="dropdown-menu">
                                <a href="http://www.cotil.unicamp.br/estagio/index.php" class="dropdown-item">Estágios</a>
                                <a href="./solicitadoc.php" class="dropdown-item">Solicitação de Documentos</a>
                                <a href="http://www.cotil.unicamp.br/arquivos/calendario_academico2017.pdf" class="dropdown-item">Calendário Acadêmico</a>
                                <a href="./aprovUniv.php" class="dropdown-item">Aprovados em Universidades</a>
                                <a href="http://www.cotil.unicamp.br/horario_aulas.php" class="dropdown-item">Horários das Aulas</a>
                                <a href="http://www.cotil.unicamp.br/arquivos/manual_do_aluno.pdf" class="dropdown-item">Manual do Aluno</a>
                                <a href="./monitorias.php" class="dropdown-item">Monitorias</a>
                            </div>
                        </li>

                        <li class="nav-item">
                            <a href="./servicos.php" class="nav-link">Serviços</a>
                        </li>

                        <li class="nav-item">
                            <a href="./concursos.php" class="nav-link">Concursos</a>
                        </li>

                        <li class="nav-item">
                            <a href="../" class="nav-link"><b style='color: #ED3237;'>Site</b></a>
                        </li>

                    </ul>

                </div>
            </div>
        </div>
    </nav>
</header>
