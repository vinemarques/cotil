<?php
session_start();
?>
<!DOCTYPE html>
<html lang="pt-BR">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" href="../assets/css/bootstrap-flex.css">
    <link rel="stylesheet" type="text/css" href="../assets/css/style.css">
    <title>Painel de Controle - COTIL</title>


    <style type="text/css">

        .row {
            margin-top: 20px;
            margin-bottom: 20px;
            padding-left: 15px;
            padding-right: 15px;
            padding-top: 15px;
            padding-bottom: 15px;
        }

        @media (min-width: 1200px)
        .row {
            max-width: 1140px;
        }

        @media (min-width: 544px)
        .row {
            max-width: 576px;
        }

    </style>

</head>
<body style="background-color: #d5d5d5">

<?php
if(isset($_REQUEST['auth']) && $_REQUEST['auth'] == true)
{
    $user = isset($_REQUEST['user'])?$_REQUEST['user']:null;
    $senha= isset($_REQUEST['senha'])?md5($_REQUEST['senha']):null;

    if(($senha=="21232f297a57a5a743894a0e4a801fc3")&&($user=="admin"))
    {
        $_SESSION['user']=$user;
        $_SESSION['authlevel']=1;

        header("Location: index.php");
    }
}
?>

<div class="top-content">
    <div class="inner-bg">
        <div class="container">

            <div class="row" style="background-color: white;">
                <div class="col-sm-6 col-sm-offset-3 form-box">

                        <h2>COTIL - Painel de Controle</h2>
                        <br>
                        <form role="form" action="?auth=true" method="post" class="login-form">
                            <div class="form-group">
                                <label for="form-user"><b>Usuário:</b></label>
                                <input type="text" name="user" placeholder="Usuário..." class="form-control" id="form-user">
                            </div>
                            <div class="form-group">
                                <label for="form-pass"><b>Senha:</b></label>
                                <input type="password" name="senha" placeholder="Senha..." class="form-control" id="form-pass">
                            </div>
                            <br>
                            <button type="submit" class="btn">Autenticar</button>
                        </form>
                </div>
            </div>
        </div>
    </div>
</div>

<script src="assets/js/jquery-3.1.1.js"></script>
<script src="assets/js/tether.js"></script>
<script src="assets/js/bootstrap.js"></script>
</body>
</html>