<?php
session_start();
?>

<!DOCTYPE html>
<html lang="pt-BR">
<head>
    <meta charset="utf-8">
    <link rel="shortcut icon" href="../assets/img/favicon.ico" type="image/x-icon">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" href="../assets/css/bootstrap-flex.css">
    <link rel="stylesheet" href="../assets/css/style.css" type="text/css" media="all">
    <script src="https://use.fontawesome.com/01f6939522.js"></script>

    <title>Painel de Controle - COTIL</title>
</head>
<body style="background-color: #d5d5d5">
    <?php
        if(!isset($_SESSION['user'])) {
        header("Location: logar.php");
        exit();
    }

    include './header.inc';
    echo "<br><br><br><br><h2 align='center' style='color: #ED3237;'>Selecione uma opção no menu acima</h2>"
?>

<script src="../assets/js/jquery-3.1.1.js"></script>
<script src="../assets/js/tether.js"></script>
<script src="../assets/js/bootstrap.js"></script>
</body>
</html>