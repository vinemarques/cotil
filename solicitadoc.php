<!DOCTYPE html>
<html lang="pt-BR">
<head>
    <meta charset="utf-8">
    <link rel="shortcut icon" href="assets/img/favicon.ico" type="image/x-icon">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" href="assets/css/bootstrap-flex.css">
    <link rel="stylesheet" href="assets/css/style.css" type="text/css" media="all">
    <script src="https://use.fontawesome.com/01f6939522.js"></script>

    <title>Colégio Técnico de Limeira</title>
</head>
<body>
<?php include './assets/include/header.inc';?>

<div class="container" style="background-color: #FFFFFF">
    <div class="jumbotron">
        <br>
        <h2 style="text-align: center">Solicitar Documentos</h2>
        <br>
        <p><b>Prazo para entrega dos documentos solicitados:</b></p>

        <p>- 2 dias úteis para alunos regularmente matriculados.</p>

        <p>- 7 dias úteis para ex-alunos.</p>

        <br>

        <p><b>Atendimento:</b></p>
        <p><b>Secretaria Acadêmica</b></p>
        <p><a href="mailto:secretaria@cotil.unicamp.br" class="text-danger">secretaria@cotil.unicamp.br</a></p>
        <p>Segunda a Sexta-feira, das 8h às 11h, das 13h às 17h e das 19h às 22h.</p>
        <p><b>Estágio</b></p>
        <p><a href="mailto:estagios@cotil.unicamp.br" class="text-danger">estagios@cotil.unicamp.br</a></p>
        <p>Segunda a Sexta-feira, das 14h às 17h e das 19h às 22h.</p>
        <br>
        <p><b>Selecione a opção desejada: </b></p>
        <div class="row">

            <div id="accordion" role="tablist" style="width: 100%;">
                <div class="card">
                    <div class="card-header" role="tab" id="Node1">
                        <h5 class="mb-0">
                            <a href="#ConteudoNode1" data-toggle="collapse" data-parent="#accordion" class="text-danger">Documentos de Estágio</a>
                        </h5>
                    </div>
                    <div class="collapse" role="tabpanel" id="ConteudoNode1">
                        <div class="card-block">
                            <br>
                            <p><b>Prazo para entrega dos documentos solicitados:</b></p>
                            <p>- 2 dias úteis para alunos regularmente matriculados.</p>
                            <p>- 7 dias úteis para ex-alunos.</p>
                            <br>
                            <form name="estagio" action="#act=estagio">
                                <div class="form-group">
                                    <label for="nome"><b>Nome:</b></label>
                                    <input type="text" class="form-control" id="nome" aria-describedby="emailHelp" placeholder="Fulano da Silva">
                                    <small id="nameHelp" class="form-text text-muted">Nome Completo.</small>
                                </div>
                                <div class="form-group">
                                    <label for="documento"><b>Insira seu RA ou RG:</b></label>
                                    <input type="text" class="form-control" id="documento" aria-describedby="docHelp" placeholder="15000 ou 555555555">
                                    <small id="docHelp" class="form-text text-muted">RG sem pontos ou traços.</small>
                                </div>
                                <div class="form-group">
                                        <label for="email"><b>Email:</b></label>
                                        <input type="email" class="form-control" id="email" aria-describedby="emailHelp" placeholder="fulano@unicamp.br">
                                        <small id="emailHelp" class="form-text text-muted">Se você não informar seu e-mail, não receberá o aviso de que seu documento está pronto.</small>
                                </div>
                                <div class="form-group">
                                        <label for="phone"><b>Telefone:</b></label>
                                        <input type="phone" class="form-control" id="phone" aria-describedby="phoneHelp" placeholder="19 999999999">
                                        <small id="phoneHelp" class="form-text text-muted">Fixo ou Móvel, acompanhando o código de área.</small>
                                </div>
                                <div class="form-group">
                                    <label for="ct"><b>Curso Técnico:</b></label>
                                    <select class="form-control" id="ct" name="ct">
                                        <option disabled selected value>Selecione um curso técnico</option>
                                        <option value="Medio">Ensino Médio</option>
                                        <option value="Informatica">Técnico em Informática</option>
                                        <option value="Edificacoes">Técnico em Edificações</option>
                                        <option value="Enfermagem">Técnico em Enfermagem</option>
                                        <option value="Geodesia">Técnico em Geodésia</option>
                                        <option value="Mecanica">Técnico em Mecânica</option>
                                        <option value="Qualidade">Técnico em Qualidade</option>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label for="periodo"><b>Periodo:</b></label>
                                    <select name="periodo" class="form-control" id="periodo">
                                        <option disabled selected value>Selecione um período</option>
                                        <option value="Diurno">Diurno</option>
                                        <option value="Noturno">Noturno</option>
                                    </select> 
                                </div>
                                <div class="form-group">
                                    <label for="serie"><b>Série:</b></label>
                                    <select name="serie" class="form-control" id="serie">
                                        <option disabled selected value>Selecione uma série</option>  
                                        <option value="1">1</option>
                                        <option value="2">2</option>
                                        <option value="3">3</option>
                                        <option value="4">4</option>
                                        <option value="concluido">Concluído</option>
                                        <option value="dependencia">Dependência</option>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label for="doc1"><b>Escolha o Documento 1:</b></label> 
                                    <select class="form-control" id="doc1" name="doc1">
                                        <option disabled selected value>Selecione um documento</option> 
                                        <option value="1"> Atestado de Estágio</option>
                                        <option value="2"> Atestado de Permissão de 8 horas de Trabalho</option>
                                        <option value="3"> Carta de Apresentação</option> 
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label for="doc2"><b>Escolha o Documento 2:</b></label>
                                    <select class="form-control" id="doc2" name="doc2">
                                        <option disabled selected value>Selecione um documento</option> 
                                        <option value="1"> Atestado de Estágio</option>
                                        <option value="2"> Atestado de Permissão de 8 horas de Trabalho</option>
                                        <option value="3"> Carta de Apresentação</option> 
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label><b>Verificação:</b></label>
                                    <div class="form-group g-recaptcha" data-sitekey="6Lf-uSEUAAAAAL99h0Y8vRHa5prs-SGMuUH7BCey""></div>
                                </div>
                                <br>
                                <button type="submit" class="btn btn-danger active">Enviar Solicitação</button>
                            </form>
                        </div>
                    </div>
                </div>

                <div class="card">
                    <div class="card-header" role="tab" id="Node2">
                        <h5 class="mb-0">
                            <a href="#ConteudoNode2" data-toggle="collapse" data-parent="#accordion" class="text-danger">Documentos da Secretaria</a>
                        </h5>
                    </div>
                    <div class="collapse" role="tabpanel" id="ConteudoNode2">
                        <div class="card-block">
                            <br>
                            <p><b>Prazo para entrega dos documentos solicitados:</b></p>
                            <p>- 2 dias úteis para alunos regularmente matriculados.</p>
                            <p>- 7 dias úteis para ex-alunos.</p>
                            <br>
                            <form name="secretaria" action="#act=secretaria">
                                <div class="form-group">
                                    <label for="nome"><b>Nome:</b></label>
                                    <input type="text" class="form-control" id="nome" aria-describedby="emailHelp" placeholder="Fulano da Silva">
                                    <small id="nameHelp" class="form-text text-muted">Nome Completo.</small>
                                </div>
                                <div class="form-group">
                                    <label for="documento"><b>Insira seu RA ou RG:</b></label>
                                    <input type="text" class="form-control" id="documento" aria-describedby="docHelp" placeholder="15000 ou 555555555">
                                    <small id="docHelp" class="form-text text-muted">RG sem pontos ou traços.</small>
                                </div>
                                <div class="form-group">
                                        <label for="email"><b>Email:</b></label>
                                        <input type="email" class="form-control" id="email" aria-describedby="emailHelp" placeholder="fulano@unicamp.br">
                                        <small id="emailHelp" class="form-text text-muted">Se você não informar seu e-mail, não receberá o aviso de que seu documento está pronto.</small>
                                </div>
                                <div class="form-group">
                                        <label for="phone"><b>Telefone:</b></label>
                                        <input type="phone" class="form-control" id="phone" aria-describedby="phoneHelp" placeholder="19 999999999">
                                        <small id="phoneHelp" class="form-text text-muted">Fixo ou Móvel, acompanhando o código de área.</small>
                                </div>
                                <div class="form-group">
                                    <label for="ct"><b>Curso Técnico:</b></label>
                                    <select class="form-control" id="ct" name="ct">
                                        <option disabled selected value>Selecione um curso técnico</option>
                                        <option value="Medio">Ensino Médio</option>
                                        <option value="Informatica">Técnico em Informática</option>
                                        <option value="Edificacoes">Técnico em Edificações</option>
                                        <option value="Enfermagem">Técnico em Enfermagem</option>
                                        <option value="Geodesia">Técnico Geodésia</option>
                                        <option value="Mecanica">Técnico em Mecânica</option>
                                        <option value="Qualidade">Técnico em Qualidade</option>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label for="periodo"><b>Periodo:</b></label>
                                    <select name="periodo" class="form-control" id="periodo">
                                        <option disabled selected value>Selecione um período</option>
                                        <option value="Diurno">Diurno</option>
                                        <option value="Noturno">Noturno</option>
                                    </select> 
                                </div>
                                <div class="form-group">
                                    <label for="serie"><b>Série:</b></label>
                                    <select name="serie" class="form-control" id="serie">
                                        <option disabled selected value>Selecione uma série</option>  
                                        <option value="1">1</option>
                                        <option value="2">2</option>
                                        <option value="3">3</option>
                                        <option value="4">4</option>
                                        <option value="concluido">Concluído</option>
                                        <option value="dependencia">Dependência</option>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label for="doc1"><b>Escolha o Documento 1:</b></label> 
                                    <select class="form-control" id="doc1" name="doc1">
                                        <option disabled selected value>Selecione um documento</option> 
                                        <option value="1"> Boletim</option>
                                        <option value="2"> Atestado de Matrícula</option>
                                        <option value="3"> Histórico do Médio</option>
                                        <option value="4"> Atestado de Aproveitamento</option>
                                        <option value="5"> Certificado Provisório do Ensino Médio</option>
                                        <option value="6"> Histórico do Técnico (com estágio concluído)</option>
                                        <option value="7"> Atestado para Isenção de Inscrição no Vestibular</option>
                                        <option value="8"> Certificado Provisório do Curso Técnico (com estágio concluído)</option>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label for="doc2"><b>Escolha o Documento 2:</b></label>
                                    <select class="form-control" id="doc2" name="doc2">
                                        <option disabled selected value>Selecione um documento</option> 
                                        <option value="1"> Boletim</option>
                                        <option value="2"> Atestado de Matrícula</option>
                                        <option value="3"> Histórico do Médio</option>
                                        <option value="4"> Atestado de Aproveitamento</option>
                                        <option value="5"> Certificado Provisório do Ensino Médio</option>
                                        <option value="6"> Histórico do Técnico (com estágio concluído)</option>
                                        <option value="7"> Atestado para Isenção de Inscrição no Vestibular</option>
                                        <option value="8"> Certificado Provisório do Curso Técnico (com estágio concluído)</option>
                                    </select>
                                </div>
                                <br>
                                <button type="submit" class="btn btn-danger active">Enviar Solicitação</button>
                            </form>
                        </div>
                    </div>
                </div>

                <div class="card">
                    <div class="card-header" role="tab" id="Node3">
                        <h5 class="mb-0">
                            <a href="#ConteudoNode3" data-toggle="collapse" data-parent="#accordion" class="text-danger">Andamento da Solicitação</a>
                        </h5>
                    </div>
                    <div class="collapse" role="tabpanel" id="ConteudoNode3">
                        <div class="card-block">
                            <br>
                            <form>
                                <div class="form-group">
                                    <label for="protocolo"><b>Digite o número do seu protocolo:</b></label>
                                    <input type="text" class="form-control" id="protocolo" placeholder="12345678" maxlength="6">
                                    <br>
                                    <button type="submit" class="btn btn-danger active"">Consultar</button>
                                </div>
                            </form>
                            <br>
                            <b>Dúvidas?</b> Envie em e-mail para: <a href="mailto:secretaria@cotil.unicamp.br" class="text-danger">secretaria@cotil.unicamp.br</a>.
                        </div>
                    </div>
                </div>

            </div>

        </div>

        <br>



    </div>

    </div>
</div>

<?php include './assets/include/footer.inc';?>

<script src="assets/js/jquery-3.1.1.js"></script>
<script src="assets/js/tether.js"></script>
<script src="assets/js/bootstrap.js"></script>
<script src='https://www.google.com/recaptcha/api.js'></script>
</body>
</html>
