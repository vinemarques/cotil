<!DOCTYPE html>
<html lang="pt-BR">
  <head>
    <meta charset="utf-8">
    <link rel="shortcut icon" href="assets/img/favicon.ico" type="image/x-icon">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" href="assets/css/bootstrap-flex.css">
    <link rel="stylesheet" href="assets/css/style.css" type="text/css" media="all">
    <script src="https://use.fontawesome.com/01f6939522.js"></script>   

    <title>Colégio Técnico de Limeira</title>
  </head>
    <body>
    <?php include './assets/include/header.inc';?>

    <div class="container" style="background-color: #FFFFFF">
	    <div class="jumbotron" style="text-align: justify">
            <br>
            <h2 style="text-align: center">Diretorias</h2>
            <br>
            <h3>Diretoria Geral</h3><br>
            <p>Núcleo executivo de tomada de decisão, planejamento, organização, coordenação,avaliação e integração de todas as atividades desenvolvidas no âmbito da Unidade Escolar.</p>
            <br>
            <div class="row">
            <div class="col-md-4">
            <img src="assets/img/fotosaran.jpg" width="102" height="128"><br>
            <br>Prof. Paulo Sérgio Saran<br>Diretor Geral<br>
            <a href="mailto:saran@cotil.unicamp.br" target="_blank">saran@cotil.unicamp.br</a>
                <br>
            </div><br>
            <br><br>
            <div class="col-md-4">
            <img src="assets/img/fotodaisy.jpg" width="102" height="128"><br>
            <br>Profa. Daisy Rodrigues Pommer<br>Diretora Associada<br>
            <a href="mailto:daisy@cotil.unicamp.br" target="_blank">daisy@cotil.unicamp.br</a>
            </div>
                <div class="col-md-4"></div>
            </div>
            <br><br>

            <h3>Diretoria Acadêmica</h3><br>
            <p>Organiza o programa acadêmico, observadas as disposições legais e regulamentares pertinentes.</p><br>
            <img src="assets/img/fotolurdinha.jpg" width="101" height="122"><br>
            <br>Profa. Maria de Lourdes Zaros Giraldello<br>Diretora Acadêmica<br>
            <a href="mailto:lurdinha@cotil.unicamp.br" target="_blank">lurdinha@cotil.unicamp.br</a>
            <br><br>
            <br>

            <h3>Diretoria Administrativa</h3><br>
            <p>Gerencia as atividades relativas a manutenção da infra-estrutura física e funcional da escola.</p><br>
            <img src="assets/img/fotoaugusto.jpg" width="104" height="121"><br>
            <br>Prof. Augusto César da Silveira<br>Diretor Administrativo<br>
            <a href="mailto:augusto@cotil.unicamp.br" target="_blank">augusto@cotil.unicamp.br</a>
            <br><br>
	    </div>
    </div>

    <?php include './assets/include/footer.inc';?>

    <script src="assets/js/jquery-3.1.1.js"></script>
    <script src="assets/js/tether.js"></script>
    <script src="assets/js/bootstrap.js"></script>
  </body>
  </html>