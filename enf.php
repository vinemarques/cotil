<!DOCTYPE html>
<html lang="pt-BR">
<head>
    <meta charset="utf-8">
    <link rel="shortcut icon" href="assets/img/favicon.ico" type="image/x-icon">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" href="assets/css/bootstrap-flex.css">
    <link rel="stylesheet" href="assets/css/style.css" type="text/css" media="all">
    <script src="https://use.fontawesome.com/01f6939522.js"></script>
    <style>
        p.ind{
            text-indent: 2em;
        }
    </style>

    <title>Colégio Técnico de Limeira</title>
</head>
<body>
<?php include './assets/include/header.inc';?>

<div class="container" style="background-color: #FFFFFF">
    <div class="jumbotron" style="text-align: justify">
        <br>
        <h2 style="text-align: center">Enfermagem</h2>
        <br>
        <p class="ind">Habilita tecnicamente
            profissionais em enfermagem para assistir o enfermeiro em todas as atividades
            de planejamento, orientação e supervisão, fornecendo
            a promoção, recuperação e reabilitação
            da saúde individual ou coletiva, podendo exercer suas funções
            em hospitais, centros de saúde, clínicas especializadas ou como
            instrumentador cirúrgico. </p>

        <p class="ind">O <b>Técnico
                em Enfermagem</b> pode trabalhar em todos os setores de um hospital, devendo zelar
            pela segurança, higiene e conforto do paciente; verificar sinais vitais,
            administrar medicamentos prescritos, realizar sondagens e curativos; instrumentar
            cirurgias; realizar atividade de desinfecção e esterilização;
            assistir pacientes graves em unidades especializadas, como: UTI adulto, UTI
            Pediátrica, UTI Neonatal, Unidade de tratamento de Queimaduras, Pronto
            Socorro, Centro Obstétrico e Centro Cirúrgico.</p>

        <p class="ind">Não há exigência de idade mínima para ingresso no curso. Para
            o exercício profissional é exigida a idade mínima de
            18 anos e habilitação pelo COREN ( Conselho Regional de Enfermagem
            ).</p>

        <p class="ind">Para o Curso
            Técnico em Enfermagem (em 02 anos) não é exigido o Certificado de Auxiliar de Enfermagem. O aluno pode ter concluído
            o Ensino Médio ou estar cursando a 2º ou 3º série dele.</p>
        <br>
        <br>
    </div>
</div>

<?php include './assets/include/footer.inc';?>

<script src="assets/js/jquery-3.1.1.js"></script>
<script src="assets/js/tether.js"></script>
<script src="assets/js/bootstrap.js"></script>
</body>
</html>