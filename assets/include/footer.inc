<footer id="footer">
    <div class="container" align="center">
        <div class="col-md-3 footer-box four-cell company-details">
            <div class="icon-top red-text">
                <a href="https://www.google.com.br/maps/place/Cotil+-+Unicamp/@-22.5628081,-47.4230576,17.25z/data=!4m5!3m4!1s0x0:0xe2108428886c8646!8m2!3d-22.5624139!4d-47.4249778"><i class="fa fa-map-marker" aria-hidden="true" style="color: #FE7569"></i></a>
            </div>Rua Paschoal Marmo, 1888 - Jardim Nova Itália - Limeira/SP - CEP: 13484-332 |
            <a href="https://www.google.com.br/maps/place/Cotil+-+Unicamp/@-22.5628081,-47.4230576,17.25z/data=!4m5!3m4!1s0x0:0xe2108428886c8646!8m2!3d-22.5624139!4d-47.4249778"><b>Mapa</b></a>
        </div>
        <div class="col-md-3 footer-box four-cell company-details">
            <div class="icon-top green-text">
                <i class="fa fa-envelope" aria-hidden="true" style="color: #34D293"></i>
            </div>
            <a href="mailto:cotil@cotil.unicamp.br">cotil@cotil.unicamp.br</a>
        </div>
        <div class="col-md-3 footer-box four-cell company-details">
            <div class="icon-top blue-text">
                <i class="fa fa-phone-square" aria-hidden="true" style="color: #3AB0E2"></i>
            </div><a href="tel:1935219903">19 3521-9903</a><br><a href="./contato.php">Contato</a>
        </div>
    </div>
</footer>