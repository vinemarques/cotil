<!DOCTYPE html>
<html lang="pt-BR">
<head>
    <meta charset="utf-8">
    <link rel="shortcut icon" href="assets/img/favicon.ico" type="image/x-icon">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" href="assets/css/bootstrap-flex.css">
    <link rel="stylesheet" href="assets/css/style.css" type="text/css" media="all">
    <script src="https://use.fontawesome.com/01f6939522.js"></script>
    <style>
        p.ind{
            text-indent:2em;
        }
    </style>

    <title>Colégio Técnico de Limeira</title>
</head>
<body>
<?php include './assets/include/header.inc';?>

<div class="container" style="background-color: #FFFFFF">
    <div class="jumbotron" style="text-align: justify">
        <br>
        <h2 style="text-align: center">EDUROAM</h2>
        <br>
        <p class="ind">Para acessá-la, clique na versão do sistema operacional de seu aparelho e siga as instruções das páginas:</p>
        <p style="text-align: center">
            <br>
            <br>
            <a href="http://www.ccuec.unicamp.br/ccuec/tutorial_android_eduroam">
                <img src="assets/img/android.jpg" border="0" width="100" height="81"></a>
            <a href="http://www.ccuec.unicamp.br/ccuec/iphone_ipodtouch_eduroam">
                <img src="assets/img/ios.jpg" border="0" width="180" height="71"></a>
            <a href="http://www.ccuec.unicamp.br/ccuec/tutorial_windowsXP_Vista_7">
                <img src="assets/img/windows.jpg" border="0" width="100" height="71"></a>
            <a href="http://www.ccuec.unicamp.br/ccuec/mac_os_eduroam">
                <img src="assets/img/mcos.jpg" border="0" width="180" height="71"></a>
        </p>
        <br>
        <p><b>EDUROAM - REDE UNICAMP-CONFIGURACAO - Orientações:</b></p>
        <p class="ind">Rede aberta, com acesso restrito apenas ao site com orientações para configuração dos aparelhos na rede EDUROAM</p>
        <br>
        <p><b>EDUROAM - REDE UNICAMP-VISITANTE - Orientações:</b></p>
        <p class="ind">Para acesso à rede sem fio Unicamp-Visitante (rede aberta, com Portal de Autenticação), é necessário que um funcionário ou docente atue como autorizador e crie a conta para o visitante.</p>
        <p class="ind">Clique aqui para abrir o <a href="http://www.cotil.unicamp.br/informatica/videos/Unicamp-Visitante.pdf" class="text-danger"><b>tutorial de configuração.</b></a> | Clique aqui para fazer o <a href="https://redesemfio.unicamp.br/visitante/logar.php" class="text-danger"><b> cadastro de visitante</b></a>.</p>
        <br>
        <br>
        <p><b>Dúvidas, envie um e-mail para : sau@cotil.unicamp.br.</b></p>
        <br>
        <br>
    </div>
</div>

<?php include './assets/include/footer.inc';?>

<script src="assets/js/jquery-3.1.1.js"></script>
<script src="assets/js/tether.js"></script>
<script src="assets/js/bootstrap.js"></script>
</body>
</html>