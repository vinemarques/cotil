<!DOCTYPE html>
<html lang="pt-BR">
<head>
    <meta charset="utf-8">
    <link rel="shortcut icon" href="assets/img/favicon.ico" type="image/x-icon">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" href="assets/css/bootstrap-flex.css">
    <link rel="stylesheet" href="assets/css/style.css" type="text/css" media="all">
    <script src="https://use.fontawesome.com/01f6939522.js"></script>
    <style>
        p.ind{
            text-indent: 2em;
        }
    </style>

    <title>Colégio Técnico de Limeira</title>
</head>
<body>
<?php include './assets/include/header.inc';?>

<div class="container" style="background-color: #FFFFFF">
    <div class="jumbotron" style="text-align: justify">
    <br>
        <h2 style="text-align: center">Núcleo de Informática</h2>
        <h4 style="text-align: center">Cotil- Unicamp</h4>
    <br>
        <p class="ind">Por meio da <a href="http://www.cotil.unicamp.br/informatica/norma052_2012.pdf" class="text-danger"> <b>RESOLUÇÃO GR 052/2012</b></a>, a Universidade estabelece normas e procedimentos para uso dos Recursos de Tecnologia da Informação. O usuário (aluno, professor ou funcionário) deverá estar ciente de seu conteúdo.</p>
        <br>
        <p class="text-danger"><b>IDENTIDADE INSTITUCIONAL (CADASTRO NO SISTEMA UNICAMP)</b></p>
        <br>
        <p><b>O que é a IDENTIDADE INSTITUCIONAL?</b></p>
        <p class="ind">R. Ela é muito importante, contém um "username" e "senha" que serão utilizados por você, para acessar os diversos serviços oferecidos pela Universidade, como por exemplo : <b>Eduroam, Microsoft Office 365, Google, Noticias Cotil, etc.</b></p>
        <p><b>** Não recebi minha Identidade Institucional (Username + Senha). O que faço?</b></p>
        <p class="ind">R. Envie seus dados (Matricula/RA + Nome Completo + RG + CPF) para <b>"sau@cotil.unicamp.br"</b>. Um novo cadastro será feito junto ao CCUEC-UNICAMP e você receberá por email sua identidade.</p>
        <p><b>** Como faço para alterar a senha de minha identidade?</b></p>
        <p class="ind">R. Primeiro, você precisa cadastrar um email alternativo que será utilizado para recuperar sua senha, caso precise. Para isso acesse: <a href ="https://www1.sistemas.unicamp.br/TrocarSenha/trocarsenhaesquecimento.do" class="text-danger">https://www1.sistemas.unicamp.br/TrocarSenha/trocarsenhaesquecimento.do</a></p>
        <p class="ind">Depois acesse: <a href="https://www1.sistemas.unicamp.br/TrocarSenha/trocarsenha.do" class="text-danger">https://www1.sistemas.unicamp.br/TrocarSenha/trocarsenha.do.</a></p>
        <p><b>** Esqueci a senha de minha identidade ou preciso recuperar minha senha e agora?</b></p>
        <p class="ind">R. Acesse: <a href="https://www1.sistemas.unicamp.br/TrocarSenha/trocarsenhaesquecimento.do" class="text-danger">https://www1.sistemas.unicamp.br/TrocarSenha/trocarsenhaesquecimento.do</a></p>
        <p class="ind"><b>Após a alteração da senha, caso já acesse a rede Eduroam, a mesma deverá ser reconfigurada nos aparelhos (celulares, notebooks, tablets) utilizando agora a nova senha.</b></p>
        <br>
        <p class="text-danger"><b>SERVIÇOS OFERECIDOS</b></p>
        <p><b>** Como faço para acessar e/ou obter informações sobre um serviço?</b></p>
        <p class="ind">R. Basta clicar no ícone correspondente.</p>
        <br>
        <p align="center">
            <a href="eduroam.php">
                <img src="assets/img/eduroam2.png" border="0" width="100" height="100"></a>&nbsp;&nbsp;
            <a href="https://cartilha.cert.br/" title="Segurança na Rede">
                <img src="assets/img/segrede3.jpg" border="0" width="110" height="100"></a>
            <a href="google.php">
                <img src="assets/img/googleclass.png" border="0"></a>
            <a href="googleexp.php">
                <img src="assets/img/googleexp.png" border="0" width="150" height="130"></a>
            <a href="microsoft.php">
                <img src="assets/img/office365.png" border="0" width="180" height="71"></a>
            <a href="http://www.cotil.unicamp.br/informatica/Informe.pdf">
                <img src="assets/img/cotil_app.png" border="0" width="100" height="100"></a></p>
        <p><b>Dúvidas: Envie um email para o Serviço de Atendimento ao Usuário: </b>sau@cotil.unicamp.br </p>
    </div>
</div>

<?php include './assets/include/footer.inc';?>

<script src="assets/js/jquery-3.1.1.js"></script>
<script src="assets/js/tether.js"></script>
<script src="assets/js/bootstrap.js"></script>
</body>
</html>