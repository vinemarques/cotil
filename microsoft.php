<!DOCTYPE html>
<html lang="pt-BR">
<head>
    <meta charset="utf-8">
    <link rel="shortcut icon" href="assets/img/favicon.ico" type="image/x-icon">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" href="assets/css/bootstrap-flex.css">
    <link rel="stylesheet" href="assets/css/style.css" type="text/css" media="all">
    <script src="https://use.fontawesome.com/01f6939522.js"></script>
    <style>
        p.ind{
            text-indent: 2em;
        }
    </style>

    <title>Colégio Técnico de Limeira</title>
</head>
<body>
<?php include './assets/include/header.inc';?>

<div class="container" style="background-color: #FFFFFF">
    <div class="jumbotron" style="text-align: justify">
        <br>
        <h2 style="text-align: center">MICROSOFT OFFICE 365 EDUCATION</h2>
        <br>
        <p class="ind">A Unicamp firmou um acordo com a Microsoft Brasil para estabelecer um canal de uso a toda comunidade universitária dos recursos que fazem parte do serviço MICROSOFT OFFICE 365 EDUCATION. Esse serviço consiste em um pacote de ferramentas gratuitas online e sua utilização está liberada a partir da data de hoje. O principal serviço disponibilizado será o acesso ao portal da Microsoft com todas as ferramentas associadas ao produto office365 (Word, Power Point, Excel, E-mail, Calendário, entre outros) e estará acessível para todos os usuários da Unicamp e Funcamp (alunos regulares, docentes e funcionários), que possuam um cadastro no sistema de Segurança da Unicamp (SiSe).<b> O ambiente pode ser acessado a partir do endereço <a href="http://office365.unicamp.br/" class="text-danger"> http://office365.unicamp.br/</a> O login é feito com seu usuário e senha (Identidade Institucional), que é a mesma senha utilizada para acesso à rede wireless da Unicamp (EDUROAM).</b> O Usuário deve aceitar os termos de uso do serviço da Unicamp e Microsoft. Os serviços do Microsoft Office 365 estão alocados no domínio m.unicamp.br (usuario@m.unicamp.br).</p>
        <br>
        <p class="ind"><b>Dúvidas,sugestões e relatos de ocorrências devem ser enviados estritamente para o endereço : sau@cotil.unicamp.br.</b></p>
        <br>
        <br>
        <br>
        <br>
    </div>
</div>

<?php include './assets/include/footer.inc';?>

<script src="assets/js/jquery-3.1.1.js"></script>
<script src="assets/js/tether.js"></script>
<script src="assets/js/bootstrap.js"></script>
</body>
</html>