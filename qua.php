<!DOCTYPE html>
<html lang="pt-BR">
<head>
    <meta charset="utf-8">
    <link rel="shortcut icon" href="assets/img/favicon.ico" type="image/x-icon">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" href="assets/css/bootstrap-flex.css">
    <link rel="stylesheet" href="assets/css/style.css" type="text/css" media="all">
    <script src="https://use.fontawesome.com/01f6939522.js"></script>
    <style>
        p{
            text-indent: 2em;
        }
    </style>

    <title>Colégio Técnico de Limeira</title>
</head>
<body>
<?php include './assets/include/header.inc';?>

<div class="container" style="background-color: #FFFFFF">
    <div class="jumbotron" style="text-align: justify">
        <br>
        <h2 style="text-align: center">Qualidade</h2>
        <br>
        <p>Capacita profissionais dinâmicos e empreendedores para se adaptarem à nova economia globalizada. O atual mercado de trabalho competitivo e exigente requer das empresas o aperfeiçoamento da qualidade e aumento da produtividade.</p>

        <p>O <b>Técnico em Qualidade</b> poderá atuar em qualquer área empresarial ( indústria, comércio, prestação de serviços), pois sua formação abrangente capacita-o a: implantar todas as ferramentas da Qualidade, buscando a melhoria contínua; preparar as empresas para a Certificação de Qualidade, de acordo com Normas Nacionais e Internacionais; usar de ferramentas estatísticas para análise dos processos de uma empresa e, após isso, elaborar projetos para a implantação dos sistemas da Qualidade.</p>

        <p>Criar e analisar tabelas de custo, elaborando projetos para a sua redução e para o aumento da lucratividade; auxiliar nas atividades de marketing e nas tomadas de decisões da empresa; ministrar palestras, seminários e realizar consultorias nas organizações, aplicando as ferramentas da Qualidade.</p>
        <br>
        <br>
        <br>
        <br>
        <br>

    </div>
</div>

<?php include './assets/include/footer.inc';?>

<script src="assets/js/jquery-3.1.1.js"></script>
<script src="assets/js/tether.js"></script>
<script src="assets/js/bootstrap.js"></script>
</body>
</html>